Source: emacs-jedi
Section: lisp
Priority: optional
Maintainer: Debian Emacsen team <debian-emacsen@lists.debian.org>
Uploaders: Lev Lamberov <dogsleg@debian.org>
Build-Depends: debhelper-compat (= 12),
               dh-elpa,
               elpa-auto-complete,
               elpa-epc,
               elpa-mocker,
               elpa-python-environment,
               python3,
               python3-epc,
               python3-jedi,
               python3-sexpdata,
               virtualenv
Standards-Version: 4.4.1
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-elpa
Homepage: https://github.com/tkf/emacs-jedi
Vcs-Browser: https://salsa.debian.org/emacsen-team/emacs-jedi
Vcs-Git: https://salsa.debian.org/emacsen-team/emacs-jedi.git

Package: elpa-jedi-core
Architecture: all
Depends: ${elpa:Depends},
         ${misc:Depends},
         python3,
         python3-epc,
         python3-jedi,
         python3-sexpdata,
         virtualenv
Recommends: emacs (>= 46.0)
Enhances: emacs
Description: common code of jedi.el and company-jedi.el
 The package provides a Python auto-completion package for Emacs. It
 aims at helping your Python coding in a non-destructive way. It also
 helps you to find information about Python objects, such as docstring,
 function arguments and code location.
 .
 This package contains common code of jedi.el and company-jedi.el.

Package: elpa-jedi
Architecture: all
Depends: ${elpa:Depends},
         ${misc:Depends}
Recommends: emacs (>= 46.0)
Enhances: emacs
Description: Python auto-completion for Emacs
 The package provides a Python auto-completion package for Emacs. It
 aims at helping your Python coding in a non-destructive way. It also
 helps you to find information about Python objects, such as
 docstring, function arguments and code location.
 .
 This package contains jedi.el, which relies on auto-complete.el.
